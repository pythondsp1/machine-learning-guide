Pipeline
********


Introduction
============

Pipelines takes 'a list of tranforms' along with 'one estimator at the end' as the inputs. In this chapter, we will use the 'Pipeline' to reimplement the :numref:`py_fig_kidney_pca_pandas_prepro`. 


Pipeline
========


In this section, :numref:`py_fig_kidney_pca_pandas_prepro` is reimplemented using 'Pipeline'. In :numref:`py_fig_kidney_pca_pipeline` the Pipeline 'pca' is defined at Lines 56-60. When 'pca.fit(df)' operation is applied at Line 62, the 'df' is send to Pipeline for processing and model is fit, and finally used by Line 63. This can be very handy tool when we have a chain of preprocessing. 


.. code-block:: python
    :linenos:
    :emphasize-lines: 8, 52, 54, 56-60
    :caption: Pipeline 
    :name: py_fig_kidney_pca_pipeline

    # kidney_dis.py

    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn.decomposition import PCA
    from sklearn import preprocessing
    from sklearn.pipeline import Pipeline

    # create header for dataset
    header = ['age','bp','sg','al','su','rbc','pc','pcc',
        'ba','bgr','bu','sc','sod','pot','hemo','pcv',
        'wbcc','rbcc','htn','dm','cad','appet','pe','ane',
        'classification']
    # read the dataset
    df = pd.read_csv("data/chronic_kidney_disease.arff", 
            header=None, 
            names=header
           )
    # dataset has '?' in it, convert these into NaN
    df = df.replace('?', np.nan)
    # drop the NaN
    df = df.dropna(axis=0, how="any")

    # print total samples
    # print("Total samples:", len(df))
    # print 4-rows and 6-columns
    # print("Partial data\n", df.iloc[0:4, 0:6])

    targets = df['classification'].astype('category')
    # save target-values as color for plotting
    # red: disease,  green: no disease
    label_color = ['red' if i=='ckd' else 'green' for i in targets]
    # print(label_color[0:3], label_color[-3:-1])

    # list of categorical features
    categorical_ = ['rbc', 'pc', 'pcc', 'ba', 'htn',
            'dm', 'cad', 'appet', 'pe', 'ane'
            ]

    # drop the "categorical" features
    # drop the classification column
    df = df.drop(labels=['classification'], axis=1)
    # drop using 'inplace' which is equivalent to df = df.drop()
    # df.drop(labels=categorical_, axis=1, inplace=True)

    # convert categorical features into dummy variable
    df = pd.get_dummies(df, columns=categorical_)
    # print("Partial data\n", df.iloc[0:4, 0:6]) # print partial data

    # StandardScaler: mean=0, variance=1
    # df = preprocessing.StandardScaler().fit_transform(df)

    # pca = PCA(n_components=2)

    # add list of transforms in Pipeline and finally the 'estimator'
    pca = Pipeline([ 
        ('scalar', preprocessing.StandardScaler()),
        ('dim_reduction', PCA(n_components=2))
        ])

    pca.fit(df)
    T = pca.transform(df) # transformed data
    # change 'T' to Pandas-DataFrame to plot using Pandas-plots
    T = pd.DataFrame(T) 

    # plot the data
    T.columns = ['PCA component 1', 'PCA component 2']
    T.plot.scatter(x='PCA component 1', y='PCA component 2', marker='o', 
            alpha=0.7, # opacity 
            color=label_color, 
            title="red: ckd, green: not-ckd" )
    plt.show()


