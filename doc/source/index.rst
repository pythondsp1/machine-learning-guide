.. Stochastic processes and Data mining with Python documentation master file, created by
   sphinx-quickstart on Sat Mar  4 18:02:21 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Machine Learning Guide
======================

.. admonition:: Codes and Datasets
   :class: danger
   
   The datasets and the codes of the tutorial can be downloaded from the `repository <https://bitbucket.org/pythondsp/machine-learning-guide/downloads/>`_

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   :numbered:

   sklearn/sklearn
   sklearn/multiclass
   sklearn/binary
   sklearn/regression
   sklearn/cv
   sklearn/kmeans
   sklearn/dimension
   sklearn/preprocessing
   sklearn/pipeline
   sklearn/clusterdim
   sklearn/image
   sklearn/moreex1
   sklearn/performance
   sklearn/guide


