# hill_valley.py 

# 1:hill, 0:valley

import matplotlib.pyplot as plt
import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split


f = open("data/Hill_Valley_without_noise_Training.data", 'r')
data = f.read()
f.close()

data = data.split() # split on \n
data = data[1:-1] # remove 0th row as it is header

# save data as list i.e. list of list will be created
data_list = []
for d in data:
    # split on comma
    row = d.split(",")
    data_list.append(row)

# convert list into numpy array, as it allows more direct-operations
data_list = np.array(data_list, float) 

# print("Number of samples:", len(data_list))
# print("(row, column):", data_list.shape) # 100 features + 1 target = 101

# # print the last value at row = 10
# row = 10 
# row_last_element = data_list[row][-1] # 1:hill, 0:valley
# print("data_list[{0}][100]: {1}".format(row,row_last_element)) # 1

# # plot row and row+1 i.e 10 and 11 here
# plt.subplot(2,1,1) # plot row 
# plt.plot(data_list[row][1:-1], label="row = {}".format(row))
# plt.legend() # show legends

# plt.subplot(2,1,2) # plot row+1 
# plt.plot(data_list[row+1][1:-1], label="row = {}".format(row+1))
# plt.legend() # show legends

# plt.show()


# extract targets
row_sample, col_sample = data_list.shape # extract row and columns in dataset

# features : last column i.e. target value will be removed form the dataset
features = np.zeros((row_sample, col_sample-1), float)
# target : store only last column
targets = np.zeros(row_sample, int)

for i, data in enumerate(data_list):
    targets[i] = data[-1]
    features[i] = data[:-1]
# print(targets)
# print(features)

# # recheck the plot
# row = 10
# plt.subplot(2,1,1) # plot row 
# plt.plot(features[row], label="row = {}".format(row))
# plt.legend() # show legends

# plt.subplot(2,1,2) # plot row+1 
# plt.plot(features[row + 1], label="row = {}".format(row+1))
# plt.legend() # show legends

# plt.show()


# split the training and test data
train_features, test_features, train_targets, test_targets = train_test_split(
        features, targets, 
        train_size=0.8, 
        test_size=0.2, 
        # random but same for all run, also accuracy depends on the 
        # selection of data e.g. if we put 10 then accuracy will be 1.0 
        # in this example
        random_state=23,
        # keep same proportion of 'target' in test and target data
        stratify=targets 
    )

# use LogisticRegression
classifier = LogisticRegression()
# training using 'training data'
classifier.fit(train_features, train_targets) # fit the model for training data

# predict the 'target' for 'training data'
prediction_training_targets = classifier.predict(train_features) 
self_accuracy = accuracy_score(train_targets, prediction_training_targets)
print("Accuracy for training data (self accuracy):", self_accuracy) 

# predict the 'target' for 'test data'
prediction_test_targets = classifier.predict(test_features) 
test_accuracy = accuracy_score(test_targets, prediction_test_targets)
print("Accuracy for test data:", test_accuracy)
