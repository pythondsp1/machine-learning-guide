# make_blob_ex.py

import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from figures import plot_2d_separator

X, y = make_blobs(centers=2, random_state=0)

# print('X.shape (samples x features):', X.shape)
# print('y.shape (samples):', y.shape)

# print('First 5 samples:\n', X[:5, :])
# print('First 5 labels:', y[:5])

# plt.scatter(X[y == 0, 0], X[y == 0, 1], c='red', s=40, label='0')
# plt.scatter(X[y == 1, 0], X[y == 1, 1], c='green', s=40, label='1')

# plt.xlabel('first feature')
# plt.ylabel('second feature')
# plt.legend()
# plt.show()

X_train, X_test, y_train, y_test = train_test_split(X, y, 
        test_size=0.2, 
        random_state=23, 
        stratify=y)

# Linear classifier
# cls = LogisticRegression()

# Nonlinear classifier
cls = KNeighborsClassifier()
cls.fit(X_train, y_train)
prediction = cls.predict(X_test)
score = cls.score(X_test, y_test)
print("Accuracy:", score)

plt.scatter(X_test[y_test == 0, 0], X_test[y_test == 0, 1], 
        c='red', s=40, label='0')
plt.scatter(X_test[y_test == 1, 0], X_test[y_test == 1, 1], 
        c='green', s=40, label='1')
plot_2d_separator(cls, X_test) # plot the boundary
plt.xlabel('first feature')
plt.ylabel('second feature')
plt.legend()
plt.show()
