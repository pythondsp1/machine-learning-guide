# rock_mine.py 

# 'R': Rock, 'M': Mine

import matplotlib.pyplot as plt
import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier


f = open("data/sonar.all-data", 'r')
data = f.read()
f.close()

data = data.split() # split on \n

# save data as list i.e. list of list will be created
data_list = []
for d in data:
    # split on comma
    row = d.split(",")
    data_list.append(row)

# extract targets
row_sample, col_sample = len(data_list), len(data_list[0]) 

# features : last column i.e. target value will be removed form the dataset
features = np.zeros((row_sample, col_sample-1), float)
# target : store only last column
targets = []  # targets are 'R' and 'M'

for i, data in enumerate(data_list):
    targets.append(data[-1])
    features[i] = data[:-1]
# print(targets)
# print(features)

# split the training and test data
train_features, test_features, train_targets, test_targets = train_test_split(
        features, targets, 
        train_size=0.8, 
        test_size=0.2, 
        # random but same for all run, also accuracy depends on the 
        # selection of data e.g. if we put 10 then accuracy will be 1.0 
        # in this example
        random_state=23,
        # keep same proportion of 'target' in test and target data
        stratify=targets 
    )

# select classifier 
classifier = LogisticRegression() 
# classifier = KNeighborsClassifier()

# training using 'training data'
classifier.fit(train_features, train_targets) # fit the model for training data

# predict the 'target' for 'training data'
prediction_training_targets = classifier.predict(train_features) 
self_accuracy = accuracy_score(train_targets, prediction_training_targets)
print("Accuracy for training data (self accuracy):", self_accuracy) 

# predict the 'target' for 'test data'
prediction_test_targets = classifier.predict(test_features) 
test_accuracy = accuracy_score(test_targets, prediction_test_targets)
print("Accuracy for test data:", test_accuracy)
