# dimension_ex.py

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split


# feature values 
x = np.random.randn(1000)
y = 2*x + np.random.randn(1000)
z = np.random.randn(1000)

# target values
t=len(x)*[0] # list of len(x)
for i, val in enumerate(z):
    if x[i]+y[i]+z[i] < 0:
        t[i] = 'N' # negative
    else:
        t[i] = 'P'

# create the dataframe
df = np.column_stack((x, y, z, t))
df = pd.DataFrame(df)
# print(df.head())

# dataframe for PCA : PCA can not have 'categorical' features
df_temp = df.drop(3, axis=1) # drop 'categorical' feature
pca = PCA(n_components=2) # 2 dimensional PCA
pca.fit(df_temp)
df_pca = pca.transform(df_temp)
# print(df_pca)

# assign targets and features values
# targets
targets = df[3]
# features
features = pd.concat([df[0], df[1], df[2]], axis=1)

#### Results for the without reduction case
# split the training and test data
train_features, test_features, train_targets, test_targets = train_test_split(
        features, targets,
        train_size=0.8,
        test_size=0.2,
        # random but same for all run, also accuracy depends on the
        # selection of data e.g. if we put 10 then accuracy will be 1.0
        # in this example
        random_state=23,
        # keep same proportion of 'target' in test and target data
        stratify=targets
    )

# use LogisticRegression
classifier = LogisticRegression()
# training using 'training data'
classifier.fit(train_features, train_targets) # fit the model for training data

print("Without dimensionality reduction:")
# predict the 'target' for 'training data'
prediction_training_targets = classifier.predict(train_features)
self_accuracy = accuracy_score(train_targets, prediction_training_targets)
print("Accuracy for training data (self accuracy):", self_accuracy)

# predict the 'target' for 'test data'
prediction_test_targets = classifier.predict(test_features)
test_accuracy = accuracy_score(test_targets, prediction_test_targets)
print("Accuracy for test data:", test_accuracy)


#### Results for the without reduction case
# updated features after dimensionality reduction
features = df_pca 
# split the training and test data
train_features, test_features, train_targets, test_targets = train_test_split(
        features, targets,
        train_size=0.8,
        test_size=0.2,
        # random but same for all run, also accuracy depends on the
        # selection of data e.g. if we put 10 then accuracy will be 1.0
        # in this example
        random_state=23,
        # keep same proportion of 'target' in test and target data
        stratify=targets
    )

# use LogisticRegression
classifier = LogisticRegression()
# training using 'training data'
classifier.fit(train_features, train_targets) # fit the model for training data

print("After dimensionality reduction:")
# predict the 'target' for 'training data'
prediction_training_targets = classifier.predict(train_features)
self_accuracy = accuracy_score(train_targets, prediction_training_targets)
print("Accuracy for training data (self accuracy):", self_accuracy)

# predict the 'target' for 'test data'
prediction_test_targets = classifier.predict(test_features)
test_accuracy = accuracy_score(test_targets, prediction_test_targets)
print("Accuracy for test data:", test_accuracy)
