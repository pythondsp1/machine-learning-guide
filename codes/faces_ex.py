# faces_ex.py

import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import fetch_olivetti_faces
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

# function for plotting images
def plot_images(images, total_images=20, rows=4, cols=5):
    fig = plt.figure() # create a new figure window
    for i in range(total_images): # display 20 images
        # subplot : 4 rows and 5 columns 
        img_grid = fig.add_subplot(rows, cols, i+1)
        # plot features as image
        img_grid.imshow(images[i])

faces = fetch_olivetti_faces() # download the dataset at ~/scikit_learn_data
# print("Keys:", faces.keys()) # display keys
# print("Total samples and image size:", faces.images.shape)
# print("Total samples and features:", faces.data.shape)
# print("Total samples and targets:", faces.target.shape)

images = faces.images # save images 

# note that images can not be saved as features, as we need 2D data for 
# features, whereas faces.images are 3D data i.e. (samples, pixel-x, pixel-y)
features = faces.data  # features 
targets = faces.target # targets

# # plot 10 images with 2 rows and 5 columns
# plot_images(images, 10, 2, 5) 
# plt.show()

# split the training and test data
train_features, test_features, train_targets, test_targets = train_test_split(
        features, targets,
        train_size=0.8,
        test_size=0.2,
        # random but same for all run, also accuracy depends on the
        # selection of data e.g. if we put 10 then accuracy will be 1.0
        # in this example
        random_state=23,
        # keep same proportion of 'target' in test and target data
        stratify=targets
    )

# use SVC
classifier = SVC(kernel="linear") # default kernel=rbf
# training using 'training data'
classifier.fit(train_features, train_targets) # fit the model for training data

# predict the 'target' for 'training data'
prediction_training_targets = classifier.predict(train_features)
self_accuracy = accuracy_score(train_targets, prediction_training_targets)
print("Accuracy for training data (self accuracy):", self_accuracy)

# predict the 'target' for 'test data'
prediction_test_targets = classifier.predict(test_features)
test_accuracy = accuracy_score(test_targets, prediction_test_targets)
print("Accuracy for test data:", test_accuracy)


# location of error for first 20 images in test data
print("Wrongly detected image-locations: ", end=' ')
for i in range (20):
    # if images are not same then print location of images
    if test_targets[i] != prediction_test_targets[i]:
        print(i)

# store test images in list
faces_test = []
for i in test_targets:
    # faces_test.append(images[i])
    # convert 'features' to images
    faces_test.append(np.reshape(features[i], (64, 64)))

# store predicted images in list
faces_predict = []
for i in prediction_test_targets:
    # faces_predict.append(images[i])
    # convert 'features' to images
    faces_predict.append(np.reshape(features[i], (64, 64)))

# plot the first 20 images from the list
plot_images(faces_test, total_images=20)
plot_images(faces_predict, total_images=20)
plt.show()



